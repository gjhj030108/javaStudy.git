package com.guo.service;

import org.apache.dubbo.config.annotation.Service;
import org.springframework.stereotype.Component;

//服务注册与发现

@Service  //可以被扫描，启动后自动注册到注册中心
@Component
public class TicketServiceImpl implements TicketService{
    @Override
    public String getTicket() {
        return "郭俊";
    }
}
