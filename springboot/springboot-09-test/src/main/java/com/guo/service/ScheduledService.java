package com.guo.service;

import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

@Service
public class ScheduledService {

    //在指定的时间执行方法

    /*
    0 0 14 * * ?
    30 0/5 10,18 * * ? 每天10点和18点，每隔5分钟执行一次

    */
    //cron
    //秒 分 时 日 月 周几
    @Scheduled(cron = "0/2 * * * * ?")
    public void hello(){
        System.out.println("hello，你被执行了");
    }
}
