package com.guo.service;

import com.guo.pojo.User;

public interface UserService {
    public User queryUserByName(String name);
}
