package com.kuang.dao;

import com.kuang.pojo.User;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

public interface UserMapper {

    //根据ID查询用户
    User getUserById(@Param("qid") int id);

    List<User> getUserByLimit(Map<String, Integer> map);

    List<User> getUserByRowBounds();
}

