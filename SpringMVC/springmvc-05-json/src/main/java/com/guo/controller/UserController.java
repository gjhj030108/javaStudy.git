package com.guo.controller;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.guo.pojo.User;
import com.guo.utils.JsonUtils;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@RestController
/*@Controller*/
public class UserController {
    @RequestMapping( "/j1")
    //@ResponseBody//不走视图解析器，直接返回字符串
    public String json1() throws JsonProcessingException {

        ObjectMapper mapper = new ObjectMapper();

        User user = new User("胡静1号",13,"女");

        String s = mapper.writeValueAsString(user);
        return s;
    }

    @RequestMapping( "/j2")
    public String json2() throws JsonProcessingException {

        List<User> userList = new ArrayList<User>();
        User user = new User("胡静1号",13,"女");
        User user2 = new User("胡静2号",13,"女");
        User user3 = new User("胡静3号",13,"女");
        User user4 = new User("胡静4号",13,"女");
        userList.add(user);
        userList.add(user2);
        userList.add(user3);
        userList.add(user4);

        return JsonUtils.getJson(userList);
    }

    @RequestMapping( "/j3")
    public String json3() throws JsonProcessingException {
        Date date = new Date();
        return JsonUtils.getJson(date,"yyyy-MM-dd HH:mm:ss");
    }
}
