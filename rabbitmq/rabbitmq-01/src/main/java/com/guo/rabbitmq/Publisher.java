package com.guo.rabbitmq;

import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.ConnectionFactory;

//生产者
public class Publisher {
    public static void main(String[] args) throws Exception{
        System.out.println("Publisher...");
        //配置连接参数
        ConnectionFactory factory = new ConnectionFactory();
        factory.setHost("47.113.223.57");
        factory.setPort(5672);
        factory.setUsername("guest");

        factory.setPassword("guest");
        //获取连接
        Connection connection = factory.newConnection();
        //获取Channel
        Channel channel = connection.createChannel();
        //配置队列参数
        //参数1：queue - 指定队列的名称
        //参数2：durable - 当前队列是否需要持久化,值为true时表示持久化，rabbitmq宕机或重启后，队列依然在
        //参数3：exclusive - 当前队列是否为排他队列，值为true时表示与当前连接（connection）绑定，连接关闭，队列消失，排他队列会对当前队列加锁，其他通道channel是不能访问的，否则会报异常
        //参数4：autoDelete - 当前队列是否自动删除，值为true时表示队列中的消息一旦被消费，该队列会消失
        //参数5：arguments - 指定当前队列的相关参数
        channel.queueDeclare("helloworldQueue",true,false,false,null);
        //发布消息到exchange，同时指定路由的规则
        // 参数1：指定exchange，目前测试没有创建交换机，使用""
        // 参数2：指定路由的规则，或者使用具体的队列名称
        // 参数3：指定传递的消息所携带的properties，目前测试不需要，使用null
        // 参数4：指定发布的具体消息，byte[]类型，目前测试需要，传递数据进行类型转换
        channel.basicPublish("","helloworldQueue",null,"helloworld".getBytes());
        //关闭资源
        channel.close();
        connection.close();
    }
}