package com.guo.rabbitmq;

import com.rabbitmq.client.*;
import java.io.IOException;
import java.util.concurrent.TimeoutException;

//消费者
public class Consumer {
    public static void main(String[] args)throws Exception {
        System.out.println("Consumer...");
        //配置连接参数
        ConnectionFactory factory = new ConnectionFactory();
        factory.setHost("47.113.223.57");
        factory.setPort(5672);
        factory.setUsername("guest");
        factory.setPassword("guest");
        //获取连接
        Connection connection = factory.newConnection();
        //获取Channel
        Channel channel = connection.createChannel();
        //监听队列
        DefaultConsumer defaultConsumer = new DefaultConsumer(channel){

            public void handleDelivery(String consumerTag, Envelope envelope, AMQP.BasicProperties properties, byte[] body) throws IOException {
                try{
//                    int i = 1/0;
                    System.out.println("来自生产者的消息："+new String(body));
                    //手动ACK
                    //参数1：deliveryTag:投递过来消息的标签,由MQ打的，从1开始，1,2,3...
                    //参数2：multiple：是否批量确认之前已经消费过的消息，一般为false
                    channel.basicAck(envelope.getDeliveryTag(), false);
                }catch (Exception e){
                    e.printStackTrace();
                    //参数1：deliveryTag:投递过来消息的标签,由MQ打的，从1开始，1,2,3...
                    //参数2：multiple：是否批量确认之前已经消费过的消息，一般为false
                    //第三个参数：requeue -> true表示重新放入队列，false -> 放弃该消息
                    //处理没有成功消费的消息
                    channel.basicNack(envelope.getDeliveryTag(), false,true);
                    try {
                        channel.close();
                    } catch (TimeoutException ex) {
                        throw new RuntimeException(ex);
                    }
                }



            }
        };

        //消费消息
        //参数1：queue - 指定消费哪个队列
        //参数2：autoAck - 指定是否自动ACK （true表示接收到消息后，会立即告知RabbitMQ，false表示不告知）
        //参数3：consumer - 指定消费回调
        channel.basicConsume("helloworldQueue",false,defaultConsumer);
        
        //设置延迟关闭，否则可能无法显示相关信息，消费方一般不关，目的是为了可以及时处理消息
        Thread.sleep(5000);
        
        //关闭资源
        channel.close();
        connection.close();
    }
}