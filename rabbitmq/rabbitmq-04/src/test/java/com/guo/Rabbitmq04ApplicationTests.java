package com.guo;

import com.guo.simple.SimplePublisher;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
class Rabbitmq04ApplicationTests {


    @Autowired
    private SimplePublisher simplePublisher;

    @Test
    void contextLoads() {
        simplePublisher.send();
    }

}
