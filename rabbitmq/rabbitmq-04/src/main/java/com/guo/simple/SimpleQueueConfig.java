package com.guo.simple;

import org.springframework.amqp.AmqpException;
import org.springframework.amqp.core.*;
import org.springframework.amqp.rabbit.connection.CorrelationData;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class SimpleQueueConfig {

    @Bean
    public Queue simple(){
        return new Queue("simpleQueue");
    }

    @Bean
    public MessagePostProcessor messagePostProcessor(){
        return new MessagePostProcessor() {
            @Override
            public Message postProcessMessage(Message message) throws AmqpException {
                return message;
            }

            @Override
            public Message postProcessMessage(Message message, Correlation correlation, String exchange, String routingKey) {
                //设置消息参数
                MessageProperties messageProperties = message.getMessageProperties();
                //设置发送消息时传过来的ID
                messageProperties.setCorrelationId(((CorrelationData)correlation).getId());

                System.out.println("配置类中的消息id："+((CorrelationData)correlation).getId());
                return message;
            }
        };
    }
}