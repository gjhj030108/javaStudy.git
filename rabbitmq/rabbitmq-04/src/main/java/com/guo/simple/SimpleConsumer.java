package com.guo.simple;

import com.rabbitmq.client.Channel;
import org.springframework.amqp.core.Message;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Component;

import java.util.concurrent.TimeUnit;

//消费者
@Component
public class SimpleConsumer{

    @Autowired
    private StringRedisTemplate redisTemplate;

    @RabbitListener(queues = "simpleQueue")
    public void receive(String content,Message message,Channel channel) throws Exception {
        //消息ID
        String correlationId = message.getMessageProperties().getCorrelationId();
        System.out.println("消费者中的消息ID："+correlationId);

        //setIfAbsent相当于setnx命令
        Boolean absent = redisTemplate.opsForValue()
                .setIfAbsent(correlationId, "0", 60, TimeUnit.SECONDS);
        if (absent) {
            //正常消费
            System.out.println("来自SimplePublisher的消息："+content);
            //消费成功修改redis值
            System.out.println("成功消费");
            redisTemplate.opsForValue().set(correlationId,"1");
            //手动ACK
            channel.basicAck(message.getMessageProperties().getDeliveryTag(),false);
        }
        if ("1".equals(redisTemplate.opsForValue().get(correlationId))){
            //证明该消息已经被消费，直接ACK
            System.out.println("已经消费，直接ACK");
            channel.basicAck(message.getMessageProperties().getDeliveryTag(),false);
        }

    }
}