package com.guo.simple;

import org.springframework.amqp.core.MessagePostProcessor;
import org.springframework.amqp.rabbit.connection.CorrelationData;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

//生产者
@Component
public class SimplePublisher{

    @Autowired
    private RabbitTemplate rabbitTemplate;

    @Autowired
    private MessagePostProcessor messagePostProcessor;

    public void send(){
        //发送消息的时候，需要给学习唯一标识
        CorrelationData correlationData = new CorrelationData();
        System.out.println("生产者发送消息的id：" + correlationData.getId());


        rabbitTemplate.convertAndSend("simpleQueue", (Object) "简单模式",messagePostProcessor,correlationData);
    }
}
