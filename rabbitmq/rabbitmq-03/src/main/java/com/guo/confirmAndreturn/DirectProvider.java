package com.guo.confirmAndreturn;

import org.springframework.amqp.core.FanoutExchange;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * @Author guo
 * @Date 2023 05 13 16 59
 **/
@Component
public class DirectProvider {
    @Autowired
    private RabbitTemplate rabbitTemplate;

    public void sent(){
        rabbitTemplate.convertAndSend("myDirectExchange","guort","我的消息");
    }
}
