package com.guo.confirmAndreturn;

import org.springframework.amqp.core.Binding;
import org.springframework.amqp.core.BindingBuilder;
import org.springframework.amqp.core.DirectExchange;
import org.springframework.amqp.core.Queue;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * @Author guo
 * @Date 2023 05 12 19 21
 **/

@Configuration
public class DirectConfig {

    @Bean
    public DirectExchange myDirectExchange(){
        return new DirectExchange("myDirectExchange");
    }

    @Bean
    public Queue myQueue(){
        return new Queue("myQueue");
    }


    //绑定交换机的队列
    @Bean
    public Binding bindDirectQueue1(){
        return BindingBuilder.bind(myQueue()).to(myDirectExchange()).with("guo");
    }
}
