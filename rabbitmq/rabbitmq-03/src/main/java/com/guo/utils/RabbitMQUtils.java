package com.guo.utils;

import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.ConnectionFactory;
import java.io.IOException;
import java.util.concurrent.TimeoutException;

public class RabbitMQUtils {
    /**
     * 1. 创建连接工厂（ConnectionFactory）
     * 2. 创建连接 (Connection)
     * 3. 创建通道  (Channel)
     Connection conn = connectionFactory.newConnection();
     Channel channel = conn.createChannel();
    */
    private static ConnectionFactory connectionFactory;

    static {
        connectionFactory = new ConnectionFactory();
        connectionFactory.setPort(5672);
        connectionFactory.setUsername("guest");
        connectionFactory.setPassword("guest");
        connectionFactory.setHost("47.113.223.57");
    }


    public static Connection getConnection() {
        Connection connection = null;
        try {
            connection = connectionFactory.newConnection();
        } catch (IOException ioException) {
            ioException.printStackTrace();
        } catch (TimeoutException e) {
            e.printStackTrace();
        }
        return connection;
    }

    public static void close(Channel channel, Connection connection) {
        try {
            if(null != channel) {
                channel.close();
            }
            if(null != connection){
                connection.close();
            }
        }catch (Exception ex) {
            ex.printStackTrace();
        }
    }
}