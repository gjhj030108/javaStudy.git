package com.guo.confirm;

import com.guo.utils.RabbitMQUtils;
import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;

import java.io.IOException;

/**
 * @Author guo
 * @Date 2023 05 14 17 47
 **/
public class ConfirmTest2 {
    public static void main(String[] args) throws IOException, InterruptedException {
        //获取连接
        Connection connection = RabbitMQUtils.getConnection();
        Channel channel = connection.createChannel();
        //开启发送确认模式
        channel.confirmSelect();
        for (int i = 0; i < 5; i++) {
            //发送消息
            channel.basicPublish("directExchange","order",null,"订单消息".getBytes());
        }
        //确认消息是否到达交换机，多条消息确认
        channel.waitForConfirmsOrDie();
        //关闭
        RabbitMQUtils.close(channel,connection);
    }

}
