package com.guo.confirm;

import com.guo.utils.RabbitMQUtils;
import com.rabbitmq.client.*;

import java.io.IOException;

/**
 * @Author guo
 * @Date 2023 05 14 17 47
 **/
public class ReturnTest {
    public static void main(String[] args) throws IOException, InterruptedException {
        //获取连接
        Connection connection = RabbitMQUtils.getConnection();
        Channel channel = connection.createChannel();
        //开启发送确认模式
        channel.confirmSelect();
        //发送消息
        channel.basicPublish("directExchange","order2",true,null,"订单消息".getBytes());
        //回调
        channel.addReturnListener(new ReturnListener() {
            @Override
            public void handleReturn(int i, String s, String s1, String s2, AMQP.BasicProperties basicProperties, byte[] bytes) throws IOException {
                //未到达队列
                System.out.println("未到达队列");
                System.out.println("exchange:"+s1);
                System.out.println("route:"+s2);
                System.out.println("内容:"+new String(bytes));
            }
        });
        //Thread.sleep(1000);

        //关闭
        //RabbitMQUtils.close(channel,connection);
    }

}
