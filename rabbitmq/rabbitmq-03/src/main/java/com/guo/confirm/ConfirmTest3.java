package com.guo.confirm;

import com.guo.utils.RabbitMQUtils;
import com.rabbitmq.client.Channel;
import com.rabbitmq.client.ConfirmListener;
import com.rabbitmq.client.Connection;

import java.io.IOException;

/**
 * @Author guo
 * @Date 2023 05 14 17 47
 **/
public class ConfirmTest3 {
    public static void main(String[] args) throws IOException, InterruptedException {
        //获取连接
        Connection connection = RabbitMQUtils.getConnection();
        Channel channel = connection.createChannel();
        //开启发送确认模式
        channel.confirmSelect();
        //发送消息
        channel.basicPublish("directExchange","order",null,"订单消息".getBytes());
        //回调
        channel.addConfirmListener(new ConfirmListener() {
            @Override
            public void handleAck(long l, boolean b) throws IOException {
                System.out.println("已到达交换机");
            }

            @Override
            public void handleNack(long l, boolean b) throws IOException {
                System.out.println("未到达交换机");
            }
        });
        //关闭
        RabbitMQUtils.close(channel,connection);
    }

}
