package com.guo.dead;

import com.guo.utils.RabbitMQUtils;
import com.rabbitmq.client.AMQP;
import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;

import java.util.HashMap;
import java.util.Map;

public class DeadQueueTest {

    //死信交换机
    private static final String dead_letter_exchange = "dead_letter_exchange";
    //死信路由键
    private static final String dead_letter_routing_key = "dead_letter_routing_key";
    //死信队列
    private static final String dead_letter_queue = "dead_letter_queue";

    private static final String people_exchange = "people_exchange";
    private static final String people_routing_key = "people_routing_key";
    private static final String people_queue = "people_queue";

    public static void main(String[] args) throws Exception{
        //获取连接
        Connection connection = RabbitMQUtils.getConnection();
        Channel channel = connection.createChannel();

        // 创建一个死信的交换机
        channel.exchangeDeclare(dead_letter_exchange, "direct");
        // 创建死信队列
        channel.queueDeclare(dead_letter_queue, true, false, false, null);
        // 将死信队列绑定到死信交换机，路由键为 "dead_letter_routing_key"
        channel.queueBind(dead_letter_queue, dead_letter_exchange, dead_letter_routing_key);

        //设置队列参数
        Map<String, Object> arguments = new HashMap<>();
        arguments.put("x-dead-letter-exchange", dead_letter_exchange);
        arguments.put("x-dead-letter-routing-key", dead_letter_routing_key);

        //创建当前交换机，队列以及路由键
        channel.exchangeDeclare(people_exchange, "direct");
        //最后一个参数是当前队列的属性
        channel.queueDeclare(people_queue, true, false, false, arguments);
        channel.queueBind(people_queue, people_exchange, people_routing_key);

        //设置消息的过期时间，单位：毫秒
        AMQP.BasicProperties properties = new AMQP.BasicProperties.Builder()
                .expiration("15000").build();

        //发送消息
        channel.basicPublish(people_exchange, people_routing_key, properties, "dead_message".getBytes());

        RabbitMQUtils.close(channel, connection);
    }
}