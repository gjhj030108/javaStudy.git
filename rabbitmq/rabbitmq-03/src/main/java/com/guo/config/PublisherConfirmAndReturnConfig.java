package com.guo.config;

import jakarta.annotation.PostConstruct;
import org.springframework.amqp.core.ReturnedMessage;
import org.springframework.amqp.rabbit.connection.CorrelationData;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;

/**
 * @Author guo
 * @Date 2023 05 14 20 27
 **/
@Configuration
public class PublisherConfirmAndReturnConfig implements RabbitTemplate.ReturnsCallback,RabbitTemplate.ConfirmCallback {

    @Autowired
    private RabbitTemplate rabbitTemplate;

    @PostConstruct
    public void init(){
        rabbitTemplate.setReturnsCallback(this);
        rabbitTemplate.setConfirmCallback(this);
    }


    @Override
    public void returnedMessage(ReturnedMessage returnedMessage) {
        System.out.println("未到达队列");
        System.out.println("交换机："+returnedMessage.getExchange());
        System.out.println("路由："+returnedMessage.getRoutingKey());
        System.out.println("内容："+ new String(returnedMessage.getMessage().getBody()));
    }

    @Override
    public void confirm(CorrelationData correlationData, boolean b, String s) {
        System.out.println("已到达交换机");
    }
}
