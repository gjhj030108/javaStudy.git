package com.guo;

import com.guo.confirmAndreturn.DirectProvider;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
class Rabbitmq03ApplicationTests {

    @Autowired
    private DirectProvider directProvider;
    @Test
    void contextLoads() {
        directProvider.sent();
    }

}
