package com.guo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableScheduling;

/**
 * @Author guo
 * @Date ${YEAR} ${MONTH} ${DAY} ${HOUR} ${MINUTE}
 **/

@SpringBootApplication
@EnableScheduling
public class CompensateApplication {
    public static void main(String[] args) {
        SpringApplication.run(CompensateApplication.class,args);
    }
}