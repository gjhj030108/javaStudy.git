package com.guo.utils;

import org.springframework.amqp.core.Message;
import org.springframework.amqp.core.MessageDeliveryMode;
import org.springframework.amqp.core.MessageProperties;
import org.springframework.amqp.core.MessagePropertiesBuilder;

/**
 * @Author guo
 * @Date 2023 05 17 17 35
 **/


public class buildMessage {
    public static Message buildMessage(String body, String messageId) {
        //获取MessagePropertiesBuilder对象
        MessagePropertiesBuilder messagePropertiesBuilder = MessagePropertiesBuilder.newInstance();
        //获取MessageProperties对象
        MessageProperties messageProperties = messagePropertiesBuilder.build();
        messageProperties.setMessageId(messageId);
        messageProperties.setDeliveryMode(MessageDeliveryMode.PERSISTENT);//消息持久化

        Message message = new Message(body.getBytes(), messageProperties);
        return message;
    }
}