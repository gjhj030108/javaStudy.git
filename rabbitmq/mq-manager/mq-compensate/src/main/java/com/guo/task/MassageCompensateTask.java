package com.guo.task;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.guo.dto.Msg;
import com.guo.mapper.MsgMapper;
import com.guo.utils.buildMessage;
import jakarta.annotation.Resource;
import org.springframework.amqp.rabbit.connection.CorrelationData;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.Scheduled;

import java.util.List;


/**
 *
 * 消息补偿：隔一段时间，去数据库查询未消费掉的消息，再次执行
 */

@Configuration
public class MassageCompensateTask {

    @Autowired
    private MsgMapper msgMapper;

    @Resource
    private RabbitTemplate rabbitTemplate;

    @Scheduled(cron = "10 * * * * ?")
    public void compensateTask(){
        //设置查询条件
        LambdaQueryWrapper<Msg> queryWrapper = new LambdaQueryWrapper<>();
        queryWrapper.eq(Msg::getStatus,-2);
        queryWrapper.lt(Msg::getTryCount,3);
        List<Msg> msgList = msgMapper.selectList(queryWrapper);
        //查询消息
        if (msgList.size()>0) {
            //遍历
            for (Msg msg : msgList) {
                rabbitTemplate.convertAndSend(
                        msg.getExchange(),
                        msg.getRoutingKey(),
                        buildMessage.buildMessage(msg.getContent(),msg.getId()),
                        new CorrelationData(msg.getId())
                );
                msg.setTryCount(msg.getTryCount()+1);
                msgMapper.updateById(msg);
            }
        }
    }
}