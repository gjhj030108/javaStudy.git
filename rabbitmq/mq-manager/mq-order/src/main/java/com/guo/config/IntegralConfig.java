package com.guo.config;

import com.guo.constant.DeadConstant;
import com.guo.constant.IntegralConstant;
import org.springframework.amqp.core.Binding;
import org.springframework.amqp.core.BindingBuilder;
import org.springframework.amqp.core.DirectExchange;
import org.springframework.amqp.core.Queue;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.HashMap;
import java.util.Map;

//路由模式
@Configuration
public class IntegralConfig {

    //创建队列
    @Bean
    public Queue createIntegralQueue(){

        Map<String, Object> arguments = new HashMap<>();
        arguments.put("x-dead-letter-exchange", DeadConstant.DEAD_LETTER_EXCHANGE);
        arguments.put("x-dead-letter-routing-key", DeadConstant.DEAD_LETTER_ROUTING_KEY);
        return new Queue(IntegralConstant.INTEGRAL_QUEUE,true,false,false,arguments);
    }

    //创建交换机
    @Bean
    public DirectExchange createIntegralExchange(){
        //交换机默认持久化
        return new DirectExchange(IntegralConstant.INTEGRAL_EXCHANGE);
    }

    //绑定：交换机中的消息可以发送到不同的队列
    @Bean
    public Binding bindingIntegralQueue(){
        //需要设置routingKey
        return BindingBuilder.bind(createIntegralQueue()).to(createIntegralExchange())
                .with(IntegralConstant.INTEGRAL_ROUTING_KEY);//和发送消息时的routingKey一致
    }

}