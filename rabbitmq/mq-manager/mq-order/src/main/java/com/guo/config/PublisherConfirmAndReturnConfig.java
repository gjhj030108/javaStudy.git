package com.guo.config;

import com.guo.dto.Msg;
import com.guo.mapper.MsgMapper;
import jakarta.annotation.PostConstruct;
import jakarta.annotation.Resource;
import lombok.extern.slf4j.Slf4j;
import org.springframework.amqp.core.Message;
import org.springframework.amqp.core.ReturnedMessage;
import org.springframework.amqp.rabbit.connection.CorrelationData;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;

import java.util.Date;
import java.util.HashMap;


@Slf4j
@Configuration
public class PublisherConfirmAndReturnConfig implements
        RabbitTemplate.ConfirmCallback,RabbitTemplate.ReturnsCallback {

    @Resource
    private RabbitTemplate rabbitTemplate;

    @Autowired
    private MsgMapper msgMapper;

    @PostConstruct
    public void init(){
        rabbitTemplate.setConfirmCallback(this);
        rabbitTemplate.setReturnsCallback(this);
    }

    @Override
    public void confirm(CorrelationData correlationData, boolean ack, String s) {
        //判断
        if(ack){
            log.info("已到达交换机");
            log.info("correlationData id is {}",correlationData.getId());
            //删除存到数据库中的消息
            //msgMapper.deleteById(correlationData.getId());//通过id删除
            //设置删除条件
            HashMap<String, Object> map = new HashMap<>();
            map.put("id",correlationData.getId());
            map.put("status",-1);
            //多条件删除
            msgMapper.deleteByMap(map);

        }else{
            log.info("没有到达broker，实际上消息已经保存到mysql中，也可以保存到redis中");
        }
    }

    //return机制，该方法比confirm先执行，只要未到达队列的时候才执行
    @Override
    public void returnedMessage(ReturnedMessage returnedMessage) {
        //如果消息到达队列不执行以下代码

        //消息已经从数据库被删除了
        //考虑人工干预，获取消息信息进行保存
        String exchange = returnedMessage.getExchange();
        String routingKey = returnedMessage.getRoutingKey();
        Message message = returnedMessage.getMessage();

        //创建消息对象，如果消息消费成功了，再去删除对应的消息
        Msg msg = new Msg();

        msg.setId(message.getMessageProperties().getMessageId());
        msg.setExchange(exchange);//积分对应的交换机
        msg.setRoutingKey(routingKey);//积分对象的路由的key
        msg.setContent(new String(message.getBody()));//积分的Json对象
        msg.setStatus(-2);//状态可以设置为和之前不一样
        msg.setTryCount(0);//尝试次数
        msg.setCreateTime(new Date());//时间
        //插入消息
        msgMapper.insert(msg);
        //做进一步处理：给管理员发邮件，发短信....

    }
}