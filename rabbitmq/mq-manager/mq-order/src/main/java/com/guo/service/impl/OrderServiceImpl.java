package com.guo.service.impl;

import com.alibaba.fastjson.JSON;
import com.guo.constant.IntegralConstant;
import com.guo.dto.Integral;
import com.guo.dto.Msg;
import com.guo.dto.Order;
import com.guo.mapper.MsgMapper;
import com.guo.mapper.OrderMapper;
import com.guo.service.OrderService;
import com.guo.utils.buildMessage;
import jakarta.annotation.Resource;
import org.springframework.amqp.rabbit.connection.CorrelationData;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;
import java.util.UUID;

@Service
@Transactional
public class OrderServiceImpl implements OrderService {

    @Autowired
    private OrderMapper orderMapper;

    @Autowired
    private MsgMapper msgMapper;

    @Resource
    private RabbitTemplate rabbitTemplate;

    @Override
    public void insertOrder(Order order) {
        //插入订单
        orderMapper.insert(order);

        //插入积分，是把积分信息发送到消息队列中
        Integral integral = new Integral();
        integral.setUserId(order.getUserId());//积分对应的用户id就是下订单的用户id
        integral.setScore(10L);
        integral.setMsg("购物积分");
        integral.setCreateTime(new Date());
        //把积分对象，转换为Json类型，发送到消息队列中
        String integralJson = JSON.toJSONString(integral);

        //创建消息对象，如果消息消费成功了，再去删除对应的消息
        Msg msg = new Msg();
        //分布式环境下，id必须是唯一的，解决方案：百度的uid-generator，美团开源项目Leaf
        String uuid = UUID.randomUUID().toString();
        msg.setId(uuid);
        msg.setExchange(IntegralConstant.INTEGRAL_EXCHANGE);//积分对应的交换机
        msg.setRoutingKey(IntegralConstant.INTEGRAL_ROUTING_KEY);//积分对象的路由的key
        msg.setContent(integralJson);//积分的Json对象
        msg.setStatus(-1);//状态
        msg.setTryCount(0);//尝试次数
        msg.setCreateTime(new Date());//时间
        //插入消息
        msgMapper.insert(msg);

        //发送消息，需要把Msg对象的id(就是uuid)传过来，一旦消息消费成功，还要去Msg对应表中把该消息删除
        CorrelationData correlationData = new CorrelationData(uuid);
        // -System.out.println("correlationData.getId()：" + correlationData.getId());
        //发送
        rabbitTemplate.convertAndSend(
                IntegralConstant.INTEGRAL_EXCHANGE,
                IntegralConstant.INTEGRAL_ROUTING_KEY,
                buildMessage.buildMessage(integralJson,uuid),
                correlationData
        );
    }
}