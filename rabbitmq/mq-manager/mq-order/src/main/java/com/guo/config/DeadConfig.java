package com.guo.config;

import com.guo.constant.DeadConstant;
import org.springframework.amqp.core.Binding;
import org.springframework.amqp.core.BindingBuilder;
import org.springframework.amqp.core.DirectExchange;
import org.springframework.amqp.core.Queue;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class DeadConfig {

    //创建队列
    @Bean
    public Queue createDeadQueue(){
        return new Queue(DeadConstant.DEAD_LETTER_QUEUE);
    }

    //创建交换机
    @Bean
    public DirectExchange createDeadExchange(){
        //交换机默认持久化
        return new DirectExchange(DeadConstant.DEAD_LETTER_EXCHANGE);
    }

    //绑定：交换机中的消息可以发送到不同的队列
    @Bean
    public Binding bindingDeadQueue(){
        //需要设置routingKey
        return BindingBuilder.bind(createDeadQueue()).to(createDeadExchange())
                .with(DeadConstant.DEAD_LETTER_ROUTING_KEY);//和发送消息时的routingKey一致
    }
}