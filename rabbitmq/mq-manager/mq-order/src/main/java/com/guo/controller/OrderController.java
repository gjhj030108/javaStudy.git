package com.guo.controller;

import com.guo.dto.Order;
import com.guo.service.OrderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("order")
public class OrderController {

    @Autowired
    private OrderService orderService;

    @PostMapping("insertOrder")
    public String insertOrder(@RequestBody Order order){
        orderService.insertOrder(order);
        return "success";
    }

}