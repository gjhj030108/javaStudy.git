package com.guo.service;

import com.guo.dto.Order;

public interface OrderService {
    /**
     * 插入订单
     * @param order
     */
    void insertOrder(Order order);
}