package com.guo.constant;

//设置系统中的参数
public class IntegralConstant {

    // 积分系统队列
    public final static String INTEGRAL_QUEUE = "integral_queue";

    // 积分系统交换机
    public final static String INTEGRAL_EXCHANGE = "integral_exchange";

    // 积分系统的 routing-key
    public final static String INTEGRAL_ROUTING_KEY= "integral_routing_key";
}