package com.guo.constant;

//死信
public class DeadConstant {
    //死信交换机
    public static final String DEAD_LETTER_EXCHANGE = "dead_letter_exchange";
    //死信路由键
    public static final String DEAD_LETTER_ROUTING_KEY = "dead_letter_routing_key";
    //死信队列
    public static final String DEAD_LETTER_QUEUE = "dead_letter_queue";
}