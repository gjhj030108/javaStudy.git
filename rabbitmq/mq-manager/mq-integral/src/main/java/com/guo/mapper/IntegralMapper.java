package com.guo.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.guo.dto.Integral;
import org.springframework.stereotype.Repository;

/**
 * @Author guo
 * @Date 2023 05 16 20 31
 **/

@Repository
public interface IntegralMapper extends BaseMapper<Integral> {
}
