package com.guo.service;

import cn.hutool.core.util.RandomUtil;
import com.alibaba.fastjson.JSON;
import com.guo.constant.IntegralConstant;
import com.guo.dto.Integral;
import com.guo.mapper.IntegralMapper;
import org.springframework.amqp.core.Message;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Service;

import java.util.concurrent.TimeUnit;

/**
 * @Author guo
 * @Date 2023 05 16 20 34
 **/

@Service
public class IntegralImpl {

    @Autowired
    private IntegralMapper integralMapper;

    @Autowired
    private StringRedisTemplate redisTemplate;

    //    //第一种方式
//    @RabbitListener(
//            bindings = @QueueBinding(
//                    value = @Queue(name = IntegralConstant.INTEGRAL_QUEUE,
//                    durable = "true"
//            ),
//            key = {IntegralConstant.INTEGRAL_ROUTING_KEY},
//            exchange = @Exchange(name = IntegralConstant.INTEGRAL_EXCHANGE, durable = "true")
//    ))
//    public void insertIntegral(Message message){
//        //积分信息
//        String integralJson = new String(message.getBody());
//        System.out.println(integralJson);
//
//        //类型转换
//        Integral integral = JSON.parseObject(integralJson, Integral.class);
//        System.out.println(integral);
//        //插入数据库
//        integralMapper.insert(integral);
//    }

    //第二种方式
//    @RabbitListener(queues = IntegralConstant.INTEGRAL_QUEUE)
//    public void insertIntegral(Message message) throws InterruptedException {
//        //积分信息
//        String integralJson = new String(message.getBody());
//        System.out.println(integralJson);
//
//        //运行成功
//        //类型转换
//        Integral integral = JSON.parseObject(integralJson, Integral.class);
//        //插入数据库
//        //integralMapper.insert(integral);
//
//        //测试消息重复消费
//        //直到方法执行完毕，才会ack
//        //Thread.sleep(500000);
//
//        //测试消息重试
//        System.out.println("当前系统时间："+System.currentTimeMillis());
//        //运行失败
//        throw new RuntimeException("消息消费异常...");
//    }

    @RabbitListener(queues = IntegralConstant.INTEGRAL_QUEUE)
    public void receiveIntegralMessage(Message message) {
        String messageId = message.getMessageProperties().getMessageId();
        if (!redisTemplate.hasKey(messageId)) {
            //获取积分信息
            String integralJson = new String(message.getBody());
            //类型转换
            Integral integral = JSON.parseObject(integralJson, Integral.class);
            //插入到数据库
            integralMapper.insert(integral);
            int randomInt = RandomUtil.randomInt(10, 100);
            //往redis中也存一份
            redisTemplate.opsForValue().setIfAbsent(messageId,
                    String.valueOf(randomInt), 6000, TimeUnit.SECONDS);
        }else {
            System.out.println("已消费");
        }
    }
}
