//package com.guo.simple;
//
//import com.guo.pojo.User;
//import org.springframework.amqp.rabbit.annotation.RabbitListener;
//import org.springframework.stereotype.Component;
//
////消费者
//@Component
//public class SimpleConsumer {
//
////    @RabbitListener(queues = "simpleQueue")
////    public void receive(String content){
////        System.out.println("SimpleConsumer...");
////        System.out.println("来自SimplePublisher的消息："+content);
////    }
//
//    @RabbitListener(queues = "simpleQueue")
//    public void receive(User user){
//        System.out.println("SimpleConsumer...");
//        System.out.println("来自SimplePublisher的消息："+user);
//    }
//}