package com.guo.simple;

import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

//生产者
@Component
public class SimplePublisher {

    @Autowired
    private RabbitTemplate rabbitTemplate;

    public void send(){
        rabbitTemplate.convertAndSend("simpleQueue","简单模式");
//        System.out.println("SimplePublisher...");
//        rabbitTemplate.convertAndSend("","simpleQueue",new User("张三", "123"));
    }
}
