package com.guo.simple;

import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Envelope;
import org.springframework.amqp.core.Message;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.util.concurrent.TimeoutException;

//消费者
@Component
public class SimpleAckConsumer {

    @RabbitListener(queues = "simpleQueue")
    public void receive(String content, Channel channel, Message message) throws IOException, TimeoutException {
        try{
//            int i = 1/0;
            System.out.println("SimpleConsumer...");
            System.out.println("来自SimplePublisher的消息："+content);
            //手动ACK
            //参数1：deliveryTag:投递过来消息的标签,由MQ打的，从1开始，1,2,3...
            //参数2：multiple：是否批量确认之前已经消费过的消息，一般为false
            channel.basicAck(message.getMessageProperties().getDeliveryTag(), false);
        }catch (Exception e){
            e.printStackTrace();
            //参数1：deliveryTag:投递过来消息的标签,由MQ打的，从1开始，1,2,3...
            //参数2：multiple：是否批量确认之前已经消费过的消息，一般为false
            //第三个参数：requeue -> true表示重新放入队列，false -> 放弃该消息
            //处理没有成功消费的消息
            channel.basicNack(message.getMessageProperties().getDeliveryTag(), false,true);
            channel.close();
        }
    }

//    @RabbitListener(queues = "simpleQueue")
//    public void receive(User user){
//        System.out.println("SimpleConsumer...");
//        System.out.println("来自SimplePublisher的消息："+user);
//    }
}