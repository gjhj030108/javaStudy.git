package com.guo.work;

import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.stereotype.Component;

@Component
public class WorkQueueCustomer_01 {

    @RabbitListener(queues="workQueue")
    public void receive(String content){
        System.out.println("WorkQueueCustomer_01："+content);
    }
}