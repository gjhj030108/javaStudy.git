package com.guo.work;

import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.stereotype.Component;

@Component
public class WorkQueueCustomer_02 {

    @RabbitListener(queues="workQueue")
    public void receive(String content){
        System.out.println("WorkQueueCustomer_02："+content);
    }
}