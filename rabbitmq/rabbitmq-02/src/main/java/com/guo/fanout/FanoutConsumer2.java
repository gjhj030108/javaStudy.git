package com.guo.fanout;

import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.stereotype.Component;

/**
 * @Author guo
 * @Date 2023 05 13 17 02
 **/

@Component
public class FanoutConsumer2 {

    @RabbitListener(queues = "fanoutQueue2")
    public void receive(String content){
        System.out.println("FanoutConsumer2接收来自fanoutQueue2的消息"+content);
    }
}
