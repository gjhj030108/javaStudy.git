package com.guo.fanout;

import org.springframework.amqp.core.FanoutExchange;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * @Author guo
 * @Date 2023 05 13 16 59
 **/
@Component
public class FanoutProvider {

    @Autowired
    FanoutExchange fanoutExchange;

    @Autowired
    private RabbitTemplate rabbitTemplate;

    public void sent(){
        rabbitTemplate.convertAndSend(fanoutExchange.getName(),"","发布订阅模式");
    }
}
