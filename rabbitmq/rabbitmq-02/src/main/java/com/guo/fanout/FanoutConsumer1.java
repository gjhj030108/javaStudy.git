package com.guo.fanout;

import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.stereotype.Component;

/**
 * @Author guo
 * @Date 2023 05 13 17 02
 **/

@Component
public class FanoutConsumer1 {

    @RabbitListener(queues = "fanoutQueue1")
    public void receive(String content){
        System.out.println("FanoutConsumer1接收来自fanoutQueue1的消息"+content);
    }
}
