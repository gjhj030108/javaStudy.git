package com.guo.direct;

import org.springframework.amqp.core.*;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * @Author guo
 * @Date 2023 05 12 19 21
 **/

@Configuration
public class DirectConfig {

    @Bean
    public DirectExchange directExchange(){
        return new DirectExchange("directExchange");
    }

    @Bean
    public Queue directQueue1(){
        return new Queue("directOrderQueue");
    }

    @Bean
    public Queue directQueue2(){
        return new Queue("directStoreQueue");
    }

    //绑定交换机的队列
    @Bean
    public Binding bindDirectQueue1(){
        return BindingBuilder.bind(directQueue1()).to(directExchange()).with("order");
    }

    @Bean
    public Binding bindDirectQueue2(){
        return BindingBuilder.bind(directQueue2()).to(directExchange()).with("store");
    }
}
