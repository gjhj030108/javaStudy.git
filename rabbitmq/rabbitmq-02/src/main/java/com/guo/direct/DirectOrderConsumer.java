package com.guo.direct;

import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.stereotype.Component;

/**
 * @Author guo
 * @Date 2023 05 13 17 02
 **/

@Component
public class DirectOrderConsumer {

    @RabbitListener(queues = "directOrderQueue")
    public void receive(String content){
        System.out.println("DirectOrderConsumer接收来自directOrderQueue的消息"+content);
    }
}
