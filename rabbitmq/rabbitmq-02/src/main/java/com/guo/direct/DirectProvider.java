package com.guo.direct;

import org.springframework.amqp.core.FanoutExchange;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * @Author guo
 * @Date 2023 05 13 16 59
 **/
@Component
public class DirectProvider {

    @Autowired
    FanoutExchange fanoutExchange;

    @Autowired
    private RabbitTemplate rabbitTemplate;

    public void sent(){
        rabbitTemplate.convertAndSend("directExchange","order","路由模式订单路由");
        rabbitTemplate.convertAndSend("directExchange","store","路由模式库存路由");
    }
}
