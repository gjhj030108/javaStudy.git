package com.guo.direct;

import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.stereotype.Component;

/**
 * @Author guo
 * @Date 2023 05 13 17 02
 **/

@Component
public class DirectStoreConsumer {

    @RabbitListener(queues = "directStoreQueue")
    public void receive(String content){
        System.out.println("DirectStoreConsumer接收来自directStoreQueue的消息"+content);
    }
}
