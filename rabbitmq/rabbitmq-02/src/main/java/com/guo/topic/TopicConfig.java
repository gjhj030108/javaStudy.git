package com.guo.topic;

import org.springframework.amqp.core.*;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * @Author guo
 * @Date 2023 05 12 19 21
 **/

@Configuration
public class TopicConfig {

    @Bean
    public TopicExchange topicExchange(){
        return new TopicExchange("topicExchange");
    }

    @Bean
    public Queue orderTopicQueue(){
        return new Queue("orderTopicQueue");
    }

    @Bean
    public Queue storeTopicQueue(){
        return new Queue("storeTopicQueue");
    }

    //绑定交换机的队列
    @Bean
    public Binding bindTopicQueue1(){
        //路由*号表示后边跟一个单词（路径路由），order.xxx
        return BindingBuilder.bind(orderTopicQueue()).to(topicExchange()).with("order.*");
    }

    @Bean
    public Binding bindTopicQueue2(){
        //路由#号表示后边跟多个单词（路径路由），order.xxx.ccc.vvv.
        return BindingBuilder.bind(storeTopicQueue()).to(topicExchange()).with("store.#");
    }
}
