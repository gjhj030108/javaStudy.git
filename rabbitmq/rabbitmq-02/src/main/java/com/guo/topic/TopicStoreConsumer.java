package com.guo.topic;

import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.stereotype.Component;

/**
 * @Author guo
 * @Date 2023 05 13 17 02
 **/

@Component
public class TopicStoreConsumer {

    @RabbitListener(queues = "storeTopicQueue")
    public void receive(String content){
        System.out.println("TopicStoreConsumer接收来自storeTopicQueue的消息：："+content);
    }
}
