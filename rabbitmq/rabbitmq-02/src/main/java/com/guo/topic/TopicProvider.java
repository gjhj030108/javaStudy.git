package com.guo.topic;

import org.springframework.amqp.core.FanoutExchange;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * @Author guo
 * @Date 2023 05 13 16 59
 **/
@Component
public class TopicProvider {

    @Autowired
    FanoutExchange fanoutExchange;

    @Autowired
    private RabbitTemplate rabbitTemplate;

    public void sent(){
        rabbitTemplate.convertAndSend("topicExchange","order.gj","主题模式订单路由");
        rabbitTemplate.convertAndSend("topicExchange","store.product001.p1","主题模式库存路由");
    }
}
