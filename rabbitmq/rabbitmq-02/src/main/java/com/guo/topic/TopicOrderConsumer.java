package com.guo.topic;

import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.stereotype.Component;

/**
 * @Author guo
 * @Date 2023 05 13 17 02
 **/

@Component
public class TopicOrderConsumer {

    @RabbitListener(queues = "orderTopicQueue")
    public void receive(String content){
        System.out.println("TopicOrderConsumer接收来自orderTopicQueue的消息：："+content);
    }
}
