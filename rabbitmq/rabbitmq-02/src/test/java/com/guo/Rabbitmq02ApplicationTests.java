package com.guo;

import com.guo.direct.DirectProvider;
import com.guo.fanout.FanoutProvider;
import com.guo.simple.SimplePublisher;
import com.guo.topic.TopicProvider;
import com.guo.work.WorkQueueProducer;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
class Rabbitmq02ApplicationTests {

	//测试简单队列
	@Autowired
	private SimplePublisher simplePublisher;

	@Test
	public void testSimpleQueueProducer(){
		simplePublisher.send();
	}
	//测试工作队列
//@Autowired
//private WorkQueueProducer workQueueProducer;

//	@Test
//	public void testWorkQueueProducer(){
//
//		workQueueProducer.send();
//	}

	//测试发布订阅模式
//	@Autowired
//	private FanoutProvider fanoutProvider;
//
//	@Test
//	public void testFanoutProvider(){
//		fanoutProvider.sent();
//	}
	//测试路由模式
	@Autowired
	private DirectProvider directProvider;

	@Test
	public void testDirectProvider(){
		directProvider.sent();
	}
	//测试主题模式
	@Autowired
	private TopicProvider topicProvider;

	@Test
	public void testTopicProvider(){
		topicProvider.sent();
	}
}
