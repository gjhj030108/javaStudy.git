package com.guo.config;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.PropertyAccessor;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.redis.connection.RedisConnectionFactory;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.serializer.Jackson2JsonRedisSerializer;
import org.springframework.data.redis.serializer.StringRedisSerializer;

@Configuration
public class RedisConfig {

    //固定模板，可以直接使用
    //自己定义了一份RedisTemplate
    @Bean
    @SuppressWarnings("all")
    public RedisTemplate<String, Object> redisTemplate(RedisConnectionFactory redisConnectionFactory){
        //我们为了自己开发方便，直接使用<String, Object>类型
        RedisTemplate<String, Object> template = new RedisTemplate<>();
        //默认的连接配置
        template.setConnectionFactory(redisConnectionFactory);

        //序列化配置
        //new 一个Jackson序列化对象，用于后面的设置
        Jackson2JsonRedisSerializer<Object> JsonSerializer = new Jackson2JsonRedisSerializer<>(Object.class);
        ObjectMapper objectMapper = new ObjectMapper();
        //使用ObjectMapper 进行转义
        objectMapper.setVisibility(PropertyAccessor.ALL, JsonAutoDetect.Visibility.ANY);
        objectMapper.enableDefaultTyping(ObjectMapper.DefaultTyping.NON_FINAL);
        //转义完才能使用
        JsonSerializer.setObjectMapper(objectMapper);

        //创建string的序列化
        StringRedisSerializer stringSerializer = new StringRedisSerializer();

        //string的key和hash的key都采用string的序列化
        //value都采用Jackson的序列化
        //key采用string序列化方式
        template.setKeySerializer(stringSerializer);
        //hash的key采用string序列化方式
        template.setHashKeySerializer(stringSerializer);
        //value采用Jackson序列化方式
        template.setValueSerializer(JsonSerializer);
        //hash的value采用Jackson序列化方式
        template.setHashValueSerializer(JsonSerializer);
        template.afterPropertiesSet();

        return template;
    }
}
