package com.guo.filter;

import org.springframework.cloud.gateway.filter.GatewayFilter;
import org.springframework.cloud.gateway.filter.GatewayFilterChain;
import org.springframework.http.server.reactive.ServerHttpRequest;
import org.springframework.web.server.ServerWebExchange;
import reactor.core.publisher.Mono;

/**
 * @Author guo
 * @Date 2023 04 17 18 54
 **/
public class AddHeaderGatewayFilter implements GatewayFilter {
    @Override
    public Mono<Void> filter(ServerWebExchange exchange, GatewayFilterChain chain) {
        ServerHttpRequest request = exchange.getRequest()
                                            .mutate()
                                            .header("X-Request-Color", "filter-Red")
                                            .build();
        ServerWebExchange webExchange = exchange.mutate().request(request).build();
        return chain.filter(webExchange);
    }
}
