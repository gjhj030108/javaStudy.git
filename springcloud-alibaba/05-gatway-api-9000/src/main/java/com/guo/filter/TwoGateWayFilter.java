package com.guo.filter;

import lombok.extern.slf4j.Slf4j;
import org.springframework.cloud.gateway.filter.GatewayFilter;
import org.springframework.cloud.gateway.filter.GatewayFilterChain;
import org.springframework.web.server.ServerWebExchange;
import reactor.core.publisher.Mono;

/**
 * @Author guo
 * @Date 2023 04 17 19 28
 **/

@Slf4j
public class TwoGateWayFilter implements GatewayFilter {
    @Override
    public Mono<Void> filter(ServerWebExchange exchange, GatewayFilterChain chain) {
        log.info("TwoFilter----pre");
        return chain.filter(exchange).then(
                Mono.fromRunnable(()->{
                    log.info("TwoFilter------post");
                })
        );
    }
}
