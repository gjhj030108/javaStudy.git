package com.guo.filter;

import lombok.extern.slf4j.Slf4j;
import org.springframework.cloud.gateway.filter.GatewayFilter;
import org.springframework.cloud.gateway.filter.GatewayFilterChain;
import org.springframework.web.server.ServerWebExchange;
import reactor.core.publisher.Mono;

/**
 * @Author guo
 * @Date 2023 04 17 19 28
 **/

@Slf4j
public class OneGateWayFilter implements GatewayFilter {
    @Override
    public Mono<Void> filter(ServerWebExchange exchange, GatewayFilterChain chain) {
        long startTime = System.currentTimeMillis();
        log.info("oneFilter-pre："+startTime);
        exchange.getAttributes().put("startTime",startTime);
        return chain.filter(exchange).then(
                Mono.fromRunnable(()->{
                    log.info("oneFilter------post");
                    Long startTimeAttr = (Long) exchange.getAttributes().get("startTime");
                    long elaspedTime = System.currentTimeMillis() - startTimeAttr;
                    log.info("所有过滤器执行的时间（毫秒）为："+elaspedTime);
                })
        );
    }
}
