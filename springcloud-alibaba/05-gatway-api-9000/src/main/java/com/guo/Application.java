package com.guo;

import com.guo.filter.AddHeaderGatewayFilter;
import com.guo.filter.OneGateWayFilter;
import com.guo.filter.ThreeGateWayFilter;
import com.guo.filter.TwoGateWayFilter;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.gateway.route.RouteLocator;
import org.springframework.cloud.gateway.route.builder.RouteLocatorBuilder;
import org.springframework.context.annotation.Bean;

@SpringBootApplication
public class Application {

    public static void main(String[] args) {
        SpringApplication.run(Application.class, args);
    }
    @Bean
    public RouteLocator routeLocator(RouteLocatorBuilder builder){
        return builder.routes()
                .route("my_router2",
                        ps ->ps.path("/info/**")
                                .filters(fs->fs
                                        .filter(new OneGateWayFilter())
                                        .filter(new TwoGateWayFilter())
                                        .filter(new ThreeGateWayFilter()))
                                .uri("http://localhost:7070"))
                .build();
    }

}
