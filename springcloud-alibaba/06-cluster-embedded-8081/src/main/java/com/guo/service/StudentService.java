package com.guo.service;


import com.guo.pojo.Student;

import java.util.List;

public interface StudentService {
    int saveStudent(Student student);
    int removeStudentById(int id);
    int modifyStudent(Student student);
    Student getStudentById(int id);
    List<Student> listAllStudents();

}
