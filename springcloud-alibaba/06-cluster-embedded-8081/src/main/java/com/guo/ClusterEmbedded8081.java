package com.guo;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;

@EnableDiscoveryClient
@MapperScan("com.guo.mapper")
@SpringBootApplication
public class ClusterEmbedded8081 {
    public static void main(String[] args) {
        SpringApplication.run(ClusterEmbedded8081.class, args);
    }

}
