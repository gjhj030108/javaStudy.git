package com.guo.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @Author guo
 * @Date 2023 04 14 10 23
 **/

@RestController
public class FallbackController {

    @GetMapping("/fb")
    public String fallbackHandle(){
        return "This is the Gateway Fallback";
    }

}
