package com.guo.fallback;

import com.guo.pojo.Student;

import java.util.ArrayList;
import java.util.List;

/**
 * @Author guo
 * @Date 2023 04 19 09 41
 **/
public class DepartServiceFallback {

    //降级方法
    public static Student getHandleFallback(int id){
        Student student = new Student(id,"sentinel-method-"+id);
        return student;
    }

    public static List<Student> listHandleFallback() {
        List<Student> list = new ArrayList<>();
        Student student = new Student(null,"no any depart");
        list.add(student);
        return list;
    }

}
