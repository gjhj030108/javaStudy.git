package com.guo;

import com.guo.mapper.StudentMapper;
import com.guo.pojo.Student;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.List;

@SpringBootTest(classes = Provider8081Application.class)
class Provider8081ApplicationTests {

    @Autowired
    StudentMapper studentMapper;

    @Test
    void contextLoads() {
        List<Student> users = studentMapper.selectList(null);
        users.forEach(System.out::println);
    }

}
