package com.guo;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@MapperScan("com.guo.mapper")
@SpringBootApplication
public class Provider8081Application {
    public static void main(String[] args) {
        SpringApplication.run(Provider8081Application.class, args);
    }

}
