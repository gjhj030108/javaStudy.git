package com.guo.controller;

import com.guo.pojo.Student;
import com.guo.service.StudentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/provider/student")
public class StudentController {

    @Autowired
    private StudentService service;

    @PostMapping("/save")
    public int saveHandle(@RequestBody Student student) {
        return service.saveStudent(student);
    }

    @DeleteMapping("/del/{id}")
    public int delHandle(@PathVariable("id") int id) {
        return service.removeStudentById(id);
    }

    @PutMapping("/update")
    public int updateHandle(@RequestBody Student student) {
        return service.modifyStudent(student);
    }

    @GetMapping("/get/{id}")
    public Student getHandle(@PathVariable("id") int id) {
        return service.getStudentById(id);
    }

    @GetMapping("/list")
    public List<Student> listHandle() {
        return service.listAllStudents();
    }

}
