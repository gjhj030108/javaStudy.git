package com.guo;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;

@EnableDiscoveryClient
@MapperScan("com.guo.mapper")
@SpringBootApplication
public class Stock8081Application {

    public static void main(String[] args) {
        SpringApplication.run(Stock8081Application.class, args);
    }

}
