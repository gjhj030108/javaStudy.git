package com.guo.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.guo.pojo.Stock;
import org.springframework.stereotype.Service;

/**
 * @Author guo
 * @Date 2023 05 29 20 29
 **/

public interface StockService extends IService<Stock> {
    Boolean add(Stock stock);
}
