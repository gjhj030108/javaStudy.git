package com.guo.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.conditions.query.LambdaQueryChainWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.guo.mapper.StockMapper;
import com.guo.pojo.Stock;
import com.guo.service.StockService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * @Author guo
 * @Date 2023 05 29 20 29
 **/
@Service
public class StackServiceImpl extends ServiceImpl<StockMapper, Stock> implements StockService {

    @Override
    public Boolean add(Stock stock) {
        LambdaQueryWrapper<Stock> queryWrapper = new LambdaQueryWrapper<>();
        queryWrapper.eq(Stock::getName,stock.getName());
        Stock stock1 = getOne(queryWrapper);
        stock1.setTotal(stock1.getTotal()+stock.getTotal());
        boolean b = updateById(stock1);
        if (b){
            return true;
        }
        return false;
    }
}
