package com.guo.controller;

import com.guo.mapper.StockMapper;
import com.guo.pojo.Stock;
import com.guo.service.StockService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class StockController {

    @Autowired
    private StockService stockService;

    @PostMapping("/insert")
    public Boolean saveStock(@RequestBody Stock Stock) {
        return stockService.add(Stock);
    }
}
