package com.guo.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.guo.pojo.Stock;
import org.springframework.stereotype.Repository;

/**
 * @Author guo
 * @Date 2023 03 02 11 18
 **/
@Repository
public interface StockMapper extends BaseMapper<Stock> {
}
