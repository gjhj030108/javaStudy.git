package com.guo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Consummer80Application {

    public static void main(String[] args) {
        SpringApplication.run(Consummer80Application.class, args);
    }

}
