package com.guo.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.guo.mapper.PurchaseMapper;
import com.guo.pojo.Purchase;
import com.guo.service.PurchaseService;
import org.springframework.stereotype.Service;

/**
 * @Author guo
 * @Date 2023 05 29 20 29
 **/
@Service
public class PurchaseServiceImpl extends ServiceImpl<PurchaseMapper,Purchase> implements PurchaseService {

    @Override
    public Boolean add(Purchase purchase) {
        LambdaQueryWrapper<Purchase> queryWrapper = new LambdaQueryWrapper<>();
        queryWrapper.eq(Purchase::getName,purchase.getName());
        Purchase purchase1 = getOne(queryWrapper);
        purchase1.setCount(purchase1.getCount()+purchase.getCount());
        boolean b = updateById(purchase1);
        if (b){
            return true;
        }
        return false;
    }
}
