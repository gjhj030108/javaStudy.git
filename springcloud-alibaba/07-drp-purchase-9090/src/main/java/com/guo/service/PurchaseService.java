package com.guo.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.guo.pojo.Purchase;
import com.guo.pojo.Stock;

/**
 * @Author guo
 * @Date 2023 05 29 20 29
 **/

public interface PurchaseService extends IService<Purchase> {
    Boolean add(Purchase purchase);
}
