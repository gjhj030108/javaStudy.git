package com.guo.controller;

import com.guo.pojo.Purchase;
import com.guo.pojo.Stock;
import com.guo.service.PurchaseService;
import io.seata.spring.annotation.GlobalTransactional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

@RestController
public class PurchaseController {

    @Autowired
    private RestTemplate restTemplate;

    @Autowired
    private PurchaseService purchaseService;

    @GlobalTransactional
    @PostMapping("/add/{den}")
    public Boolean savePurchase(@RequestBody Purchase purchase , @PathVariable("den") int den) {
        purchaseService.add(purchase);

        int i =3/den;
        String url = "http://ms-stock/insert";
        Stock stock = new Stock();
        stock.setName(purchase.getName());
        stock.setTotal(purchase.getCount());
        return restTemplate.postForObject(url,stock,Boolean.class);
    }
}
