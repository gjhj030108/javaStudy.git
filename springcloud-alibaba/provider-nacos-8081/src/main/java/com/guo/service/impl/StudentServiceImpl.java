package com.guo.service.impl;

import com.guo.mapper.StudentMapper;
import com.guo.pojo.Student;
import com.guo.service.StudentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class StudentServiceImpl implements StudentService {
    @Autowired
    StudentMapper studentMapper;
    @Override
    public int saveStudent(Student student) {
        return studentMapper.insert(student);
    }

    @Override
    public int removeStudentById(int id) {
        return studentMapper.deleteById(id);
    }

    @Override
    public int modifyStudent(Student student) {
        return studentMapper.insert(student);
    }

    @Override
    public Student getStudentById(int id) {
        return studentMapper.selectById(id);
    }

    @Override
    public List<Student> listAllStudents() {
        return studentMapper.selectList(null);
    }
}
