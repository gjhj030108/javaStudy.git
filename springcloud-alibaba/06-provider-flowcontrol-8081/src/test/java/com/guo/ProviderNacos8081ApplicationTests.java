package com.guo;

import com.guo.mapper.StudentMapper;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
class ProviderNacos8081ApplicationTests {

    @Autowired
    StudentMapper studentMapper;
    @Test
    void contextLoads() {
        System.out.println(studentMapper.selectById(1));
    }

}
