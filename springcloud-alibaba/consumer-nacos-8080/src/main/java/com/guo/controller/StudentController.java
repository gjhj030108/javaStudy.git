package com.guo.controller;

import com.guo.pojo.Student;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.client.RestTemplate;

import java.util.List;

/**
 * @Author guo
 * @Date 2023 02 27 20 42
 **/
@RestController
public class StudentController {

    @Autowired
    private RestTemplate restTemplate;

    private static final String url = "http://student-provider";

    @PostMapping("/save")
    public boolean saveHandle(@RequestBody Student student) {
        return restTemplate.postForObject(url+"/save", student, Boolean.class);
    }

    @DeleteMapping("/del/{id}")
    public void delHandle(@PathVariable("id") int id) {
        restTemplate.delete(url+"/del/"+id);
    }

    @PutMapping("/update")
    public void updateHandle(@RequestBody Student student) {
        restTemplate.put(url+"/update", student);
    }

    @GetMapping("/get/{id}")
    public Student getHandle(@PathVariable("id") int id) {
        return restTemplate.getForObject(url+"/get/"+id, Student.class);
    }

    @GetMapping("/list")
    public List<Student> listHandle() {
        return restTemplate.getForObject(url+"/list",List.class);
    }
}
