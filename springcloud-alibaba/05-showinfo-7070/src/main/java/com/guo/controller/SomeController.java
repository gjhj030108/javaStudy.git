package com.guo.controller;

import jakarta.servlet.http.HttpServletRequest;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Enumeration;

/**
 * @Author guo
 * @Date 2023 04 13 19 31
 **/

@RestController
@RequestMapping("/info")
public class SomeController {

    @GetMapping("/params")
    public String paramsHandle(String red){
        return red;
    }

    @GetMapping("/time")
    public String timeHandle(){
        return "请求到达服务器的时间："+System.currentTimeMillis();
    }

    @GetMapping("/headers")
    public String headersHandle(HttpServletRequest request){
        StringBuilder sb = new StringBuilder();
        //获取指定key的所有value
        Enumeration<String> headers = request.getHeaders("X-Request-Color");
        //遍历header的所有value
        while (headers.hasMoreElements()) {
            //将遍历的value追加到StringBuilder中
            sb.append(headers.nextElement()+"");
        }

        return "X-Request-Color:"+sb.toString();
    }

    @GetMapping("/allHeaders")
    public String allHeadersHandle(HttpServletRequest request){
        StringBuilder sb = new StringBuilder();
        //获取请求头所有的key
        Enumeration<String> headerNames = request.getHeaderNames();
        //遍历所有key
        while (headerNames.hasMoreElements()) {
            //获取key
            String name = headerNames.nextElement();
            sb.append(name+"===");
            //获取当前key的所有value
            Enumeration<String> headers = request.getHeaders(name);
            //遍历所有value
            while (headers.hasMoreElements()) {
                //将当前遍历的value追加到sb中
                sb.append(headers.nextElement()+"");
            }
            sb.append("<br>");

        }

        return sb.toString();
    }

    @GetMapping("/header")
    public String headerHandle(HttpServletRequest request){
        String header = request.getHeader("X-Request-Color");
        return "X-Request-Color:"+header;
    }
}
