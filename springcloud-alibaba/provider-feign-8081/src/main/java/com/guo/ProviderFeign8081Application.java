package com.guo;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;

@EnableDiscoveryClient
@MapperScan("com.guo.mapper")
@SpringBootApplication
public class ProviderFeign8081Application {

    public static void main(String[] args) {
        SpringApplication.run(ProviderFeign8081Application.class, args);
    }

}
