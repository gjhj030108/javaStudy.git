package com.guo.service.impl;

import com.guo.mapper.StudentMapper;
import com.guo.pojo.Student;
import com.guo.service.StudentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class StudentServiceImpl implements StudentService {
    @Autowired
    StudentMapper studentMapper;
    @Override
    public int saveStudent(Student student) {
        return studentMapper.insert(student);
    }

    @Override
    public int removeStudentById(int id) {
        return studentMapper.deleteById(id);
    }

    @Override
    public int modifyStudent(Student student) {
        return studentMapper.insert(student);
    }

    @Override
    public Student getStudentById(int id) {
        return studentMapper.selectById(id);
    }

    @Value("${server.port}")
    private int port;

    @Override
    public List<Student> listAllStudents() {
        ArrayList<Student> list = new ArrayList<>();
        Student student = new Student();
        student.setName("端口号");
        student.setId(port);
        list.add(student);
        return list;
    }
}
