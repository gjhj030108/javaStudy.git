package com.guo;

import com.guo.service.StudentService;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
class ConsumerFeign8080ApplicationTests {
    @Autowired
    StudentService studentService;
    @Test
    void contextLoads() {
        studentService.getStudentById(1);
    }

}
