package com.guo.controller;

import com.guo.pojo.Student;
import com.guo.service.StudentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * @Author guo
 * @Date 2023 02 27 20 42
 **/
@RestController
public class StudentController {

    @Autowired
    StudentService studentService;

    @PostMapping("/save")
    public int saveHandle(@RequestBody Student student) {
        return studentService.saveStudent(student);
    }

    @DeleteMapping("/del/{id}")
    public int delHandle(@PathVariable("id") int id) {
        return studentService.removeStudentById(id);
    }

    @PutMapping("/update")
    public void updateHandle(@RequestBody Student student) {
        studentService.modifyStudent(student);
    }

    @GetMapping("/get/{id}")
    public Student getHandle(@PathVariable("id") int id) {
        return studentService.getStudentById(id);
    }

    @GetMapping("/list")
    public List<Student> listHandle() {
        System.out.println("hello");
        return studentService.listAllStudents();
    }
}
