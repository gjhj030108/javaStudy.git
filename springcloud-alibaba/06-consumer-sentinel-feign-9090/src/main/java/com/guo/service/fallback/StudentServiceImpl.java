package com.guo.service.fallback;

import com.guo.pojo.Student;
import com.guo.service.StudentService;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.List;

/**
 * @Author guo
 * @Date 2023 04 19 10 00
 **/
@RequestMapping("/fallback")
@Component
public class StudentServiceImpl implements StudentService {
    @Override
    public int saveStudent(Student student) {
        System.out.println("执行saveStudent()的服务降级处理方法");
        return 0;
    }

    @Override
    public int removeStudentById(int id) {
        System.out.println("执行removeStudentById()的服务降级处理方法");
        return 0;
    }

    @Override
    public int modifyStudent(Student student) {
        System.out.println("执行modifyStudent()的服务降级处理方法");
        return 0;
    }

    @Override
    public Student getStudentById(int id) {
        System.out.println("执行getStudentById()的服务降级处理方法");
        Student student = new Student(null, "openfeign-degrade");
        return student;
    }

    @Override
    public List<Student> listAllStudents() {
        System.out.println("执行listAllStudents()的服务降级处理方法");
        return null;
    }
}
