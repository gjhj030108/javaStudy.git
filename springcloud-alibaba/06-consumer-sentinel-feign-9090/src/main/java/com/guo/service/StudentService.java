package com.guo.service;


import com.guo.pojo.Student;
import com.guo.service.fallback.StudentServiceImpl;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Component
@FeignClient(value = "student-provider",
             fallback = StudentServiceImpl.class)
public interface StudentService {

    @PostMapping("/save")
    int saveStudent(@RequestBody Student student);

    @DeleteMapping("/del/{id}")
    int removeStudentById(@PathVariable("id") int id);

    @PutMapping("/update")
    int modifyStudent(@RequestBody Student student);

    @GetMapping("/get/{id}")
    Student getStudentById(@PathVariable("id") int id);

    @GetMapping("/list")
    List<Student> listAllStudents();

}
