package com.guo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.openfeign.EnableFeignClients;

@EnableFeignClients("com.guo.service")
@SpringBootApplication
public class ConsumerFeign8080Application {

    public static void main(String[] args) {
        SpringApplication.run(ConsumerFeign8080Application.class, args);
    }

}
