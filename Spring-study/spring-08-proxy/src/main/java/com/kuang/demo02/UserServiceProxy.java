package com.kuang.demo02;

public class UserServiceProxy implements UserService{
    private UserService userService;

    public void setUserService(UserService userService) {
        this.userService = userService;
    }

    @Override
    public void add() {
        log("add");
        userService.add();
    }

    @Override
    public void delete() {
        log("delete");
        userService.add();
    }

    @Override
    public void update() {
        log("opdate");
        userService.add();
    }

    @Override
    public void query() {
        log("opdate");
        userService.add();
    }
    //日志方法
    public void log(String msg){
        System.out.println("[debug]使用了"+msg+"方法");
    }
}
