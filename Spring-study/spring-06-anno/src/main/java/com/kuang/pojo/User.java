package com.kuang.pojo;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;
//等价于<bean id="User" class="com.kuang.pojo.User"/>
@Component
//@Scope("prototype")
public class User {
    public String name;
    //相当于<property name="name" value="郭俊"/>
    @Value("郭俊")
    public void setName(String name) {
        this.name = name;
    }
}
