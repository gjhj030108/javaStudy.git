package com.kuang.config;

import com.kuang.pojo.User;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;

//也会被托管，注册到容器中，本来就是一个Component
//类似beans.xml
@Configuration
@ComponentScan("com.kuang.pojo")
//导入配置
@Import(kuangconfig2.class)
public class kuangconfig {
    //注册一个bean，相当于之前的bean标签
    //这个方法的名字相当于id
    //返回值相当于class属性
    @Bean
    public User user(){
        return new User();//返回要注入到bean的对象！
    }
}
