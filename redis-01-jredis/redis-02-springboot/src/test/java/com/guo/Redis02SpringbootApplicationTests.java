package com.guo;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.guo.pojo.User;
import com.guo.utils.RedisUtil;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.redis.core.RedisTemplate;

@SpringBootTest
class Redis02SpringbootApplicationTests {
    @Autowired
    @Qualifier("redisTemplate")
    private RedisTemplate redisTemplate;

    @Test
    void contextLoads() {

        //redisTemplate
        //opsForValue  操作字符串
        //opsForList   操作List
        //RedisConnection connection = redisTemplate.getConnectionFactory().getConnection();
        //connection.flushAll();
        redisTemplate.opsForValue().set("mykey","郭俊");
        System.out.println(redisTemplate.opsForValue().get("mykey"));
    }

    @Autowired
    private RedisUtil redisUtil;

    @Test
    public void test() throws JsonProcessingException {
        //真实开发一般都使用json来传递对象
        User user = new User("郭俊", 18);
/*        String jsonUser = new ObjectMapper().writeValueAsString(user);*/
        redisUtil.set("user",user);
        System.out.println(redisUtil.get("user"));
    }

}
